package thonn.datarest.springdataresthomework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import thonn.datarest.springdataresthomework.entity.Author;
import thonn.datarest.springdataresthomework.entity.Book;
import thonn.datarest.springdataresthomework.repositoy.AuthorRepository;
import thonn.datarest.springdataresthomework.repositoy.BookRepositoy;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Controller
@Transactional
public class BookController {

    @Autowired
    private BookRepositoy bookRepositoy;

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping("/add")
    @ResponseBody
    public String addBook(){

        Author author=new Author("Jonh");
        Author author1=new Author("Sao");
        Author author2=new Author("Nila");

        Book book=new Book("Conversation");
        book.getAuthors().add(author);
        author.getBooks().add(book);
        author1.getBooks().add(book);


        Book book1=new Book("Writing");
        book1.getAuthors().add(author2);
        author2.getBooks().add(book1);
        author2.getBooks().add(book);

//        bookRepositoy.save(book);
//        bookRepositoy.save(book1);

        authorRepository.save(author);
        authorRepository.save(author1);
        authorRepository.save(author2);


        return "true";
    }

}
