package thonn.datarest.springdataresthomework.repositoy;

import org.springframework.data.repository.CrudRepository;
import thonn.datarest.springdataresthomework.entity.Book;

public interface BookRepositoy extends CrudRepository<Book,Integer> {
}
