package thonn.datarest.springdataresthomework.repositoy;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import thonn.datarest.springdataresthomework.entity.Author;

import java.util.List;

@RepositoryRestResource(itemResourceRel = "/api/v3")
public interface AuthorRepository extends CrudRepository<Author,Integer> {
    /**
     * find all author
     * authors/search/all-author
     */
    @RestResource(path = "all-author")
    public List<Author> findAuthorBy();


    /**
     * search by author_id
     * authors/seach/findAuthorById?id=id
     */
    public List<Author> findAuthorById(@Param("id")Integer id);

    /**
     * delete find author_id
     */
    public Author deleteAllById(@Param("id")Integer id);


    /**
     * for /{repository}/{id}/{property}/{propertyId}
     * find by relationaship entity
     */
}
