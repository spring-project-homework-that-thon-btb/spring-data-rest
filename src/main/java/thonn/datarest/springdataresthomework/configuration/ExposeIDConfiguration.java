package thonn.datarest.springdataresthomework.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import thonn.datarest.springdataresthomework.entity.Author;
import thonn.datarest.springdataresthomework.entity.Book;

@Configuration
public class ExposeIDConfiguration extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Author.class);
        config.exposeIdsFor(Book.class);
        config.setBasePath("/api/v3");
    }
}
