package thonn.datarest.springdataresthomework;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import thonn.datarest.springdataresthomework.entity.Author;
import thonn.datarest.springdataresthomework.entity.Book;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class SpringDatarestHomeworkApplication{

	public static void main(String[] args) {
		SpringApplication.run(SpringDatarestHomeworkApplication.class, args);
	}


}

